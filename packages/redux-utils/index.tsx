"use client";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import type { AppState, AppDispatch } from "./src/store";
import {
  increment,
  incrementByAmount,
  decrement,
  decrementByAmount,
  reset,
} from "./src/counterSlice";

export { store } from "./src/store";
// export { pokemonApi } from "./src/api";
export { Provider } from "react-redux";

export { increment, incrementByAmount, decrement, decrementByAmount, reset };

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector;
