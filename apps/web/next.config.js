module.exports = {
  reactStrictMode: true,
  transpilePackages: ['ui', 'redux-utils', 'hooks-and-utils', 'math-helpers'],
};
