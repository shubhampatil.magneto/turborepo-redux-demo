"use client";
import { Button, Header } from "ui";
import { add } from "math-helpers";
import { decrement, increment, reset } from "redux-utils";
import { useAppDispatch, useAppSelector } from "redux-utils";

export default function Page() {
  const count = useAppSelector((state) => state.counterReducer.value);
  const dispatch = useAppDispatch();

  return (
    <>
      <main style={{ maxWidth: 1200, marginInline: "auto", padding: 20 }}>
        <div style={{ marginBottom: "4rem", textAlign: "center" }}>
          <Header text="Web" />
          <Button />
          <br />
          <h5>{add(10, 20)}</h5>
          <h4 style={{ marginBottom: 16 }}>{count}</h4>
          <button onClick={() => dispatch(increment())}>increment</button>
          <button
            onClick={() => dispatch(decrement())}
            style={{ marginInline: 16 }}
          >
            decrement
          </button>
          <button onClick={() => dispatch(reset())}>reset</button>
        </div>
      </main>
    </>
  );
}
