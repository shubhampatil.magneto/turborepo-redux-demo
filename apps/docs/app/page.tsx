"use client";
import { Button, Header } from "ui";
import { decrement, increment, reset } from "redux-utils";
import { useAppDispatch, useAppSelector } from "redux-utils";

export default function Page() {
  const count = useAppSelector((state) => state.counterReducer.value);
  const dispatch = useAppDispatch();

  return (
    <>
      <Header text="Docs" />
      <h4 style={{ marginBottom: 16 }}>{count}</h4>
      <button onClick={() => dispatch(increment())}>increment</button>
      <button
        onClick={() => dispatch(decrement())}
        style={{ marginInline: 16 }}
      >
        decrement
      </button>
      <button onClick={() => dispatch(reset())}>reset</button>
      <Button />
    </>
  );
}
